<?php
class Members extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'members';
	}

	public function relations(){
		return array(
			'my_qualification' => array(
					self::HAS_ONE, 'M_Qualification', 'memberid'
			),
			'my_request' => array(
					self::HAS_ONE, 'M_Request', 'memberid'
			),
			'mj_category' => array(
					self::BELONGS_TO, 'J_Category', 'job_category_id'
			),
			'mjob_request' => array(
					self::HAS_ONE, 'Job_Request', 'memberid'
			)
		);
	}
}