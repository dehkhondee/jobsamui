<?php
class Interest extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'job_interest';
	}

	public function relations(){
		return array(
				'm_member' => array(self::BELONGS_TO, 'Members', 'memberid')
			);
	}

}