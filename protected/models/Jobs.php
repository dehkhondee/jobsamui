<?php
class Jobs extends CActiveRecord
{

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'jobs';
	}
	public function relations(){
		return array(
			'my_company' => array(
					self::BELONGS_TO, 'Company', 'companyid'
			) 

		);
	}
}