<?php

class memberController extends Controller
{
	
	public function actionIndex()
	{
		$j_category = J_Category::model()->findAll();
		$this->render('//site/member/login', array(
				'j_category' => $j_category
			));
	}
	public function actionLogin()
	{
		$j_category = J_Category::model()->findAll();
		$this->render('//site/member/login', array(
				'j_category' => $j_category
			));
	}
	public function actionRegister(){
		$j_category = J_Category::model()->findAll();
		$this->render('//site/member/register', array(
				'j_category' => $j_category
			));
	}
	public function actionResume(){
		$id_member = Yii::app()->session['id_member'];
		$resultmember = Members::model()->findByPk($id_member);
		$j_category = J_Category::model()->findAll();
		$this->render('//site/member/resume', array(
				'j_category' => $j_category,
				'resultmem' => $resultmember
			));
		
	}
	public function actionChecklogin(){

		$txtemail = $_REQUEST['txtusername'];
		$txtpass = $_REQUEST['txtpassword'];

		$criteria = new CDbCriteria();
			$criteria->condition = "
				email = :u AND
				password = :p
			";
			$criteria->params = array(
				'u' => $txtemail,
				'p' => $txtpass
			);
			$checkuser = Members::model()->find($criteria);

			if(!empty($checkuser)){
				Yii::app()->session['id_member'] = $checkuser->id_member;
				Yii::app()->session['email'] = $checkuser->email;
				Yii::app()->session['name'] = $checkuser->name;
				echo "ok";
				//echo CJSON::encode($checkuser);
			}else{
				//echo CJSON::encode(array(
				//		"message" => "Username invalid"
				//	));
			}

	}
	public function actionFileupload(){
		$this->render('//site/member/fileupload');
	}
	public function actionUploadimage(){
		
		$imagename = time()."-".$_FILES['file']['name'];
		
		if(move_uploaded_file($_FILES['file']['tmp_name'], 'upload/'.$imagename)){
			$memid = $_POST['memid'];
			$member = Members::model()->findByPk($memid);
			$member->image = $imagename;
			$member->save();
			
			echo "<img src='../upload/$imagename' style='width:120px;'/>";
			
		}
	}
	public function actionCheckuser(){
		$txtemail = $_REQUEST['email'];
		$criteria = new CDbCriteria();
			$criteria->condition = "
				email = :u 
			";
			$criteria->params = array(
				'u' => $txtemail
			);
			$checkuser = Members::model()->find($criteria);
			if(empty($checkuser)){
				echo "ok";
			}else{
				echo "no";
			}
	}
	public function actionRegistersave(){
		if(!empty($_POST)){
				foreach($_POST as $key => $value){
					$answer[$key] = $value;
				}
			$connection = Yii::app()->db;
			$connection->active = true;

			$sql = "SELECT max(id_member) FROM members";
			$command = $connection->createCommand($sql);
			$dataReader = $command->query();
			foreach($dataReader as $row){}
			$connection->active = false;
			$tmpID = substr($row['max(id_member)'], 1);
			$tmpID++;
			if($tmpID<=10){
				$NewID = "M".str_pad($tmpID, 3, "0", STR_PAD_LEFT);
			}else{
				$NewID = "M".str_pad($tmpID, 3, "0", STR_PAD_LEFT);
			}
				$member = new Members();
				$member->id_member = $NewID;
				$member->register_date = new CDbExpression('NOW()');
				$member->email = $answer['txtemail'];
				$member->password = $answer['txtpassword'];
				$member->name = $answer['txtname'];
				$member->surname = $answer['txtsurname'];
				$member->address = $answer['txtaddress'];
				$member->phone = $answer['txttel'];
				$member->save();
			echo "ok";
		}
	}
}