<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$criteria = new CDbCriteria();
			$criteria->condition = "
				status = :u 
			";
			$criteria->params = array(
				'u' => '1'
			);
		$j_category = J_Category::model()->findAll();
		$article = Article::model()->findAll();
		$jobs = Jobs::model()->findAll($criteria);
		$this->render('//site/main', array(
				'j_category' => $j_category,
				'article' => $article,
				'jobs' => $jobs
			));
	}

	

}