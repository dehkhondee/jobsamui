<?php

class jobsController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'

			
			$id = $_GET['id'];
			$criteria = new CDbCriteria();
			$criteria->condition = "
				id_jobs = :u 
			";
			$criteria->params = array(
				'u' => $id
			);
			$j_category = J_Category::model()->findAll();
			$article = Article::model()->findAll();
			$job = Jobs::model()->findByPk($id);
			
			//$rowjob = CJSON::encode($job);
			
			//echo print_r($result);
			$this->render('//site/jobs', array(
					'j_category' => $j_category,
					'article' => $article,
					'job' => $job
				));
		
	}

}