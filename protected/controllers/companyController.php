<?php

class companyController extends Controller
{

	public function actionIndex()
	{
		$j_category = J_Category::model()->findAll();
		$this->render('//site/company/login', array(
				'j_category' => $j_category
			));
	}

	public function actionLogin()
	{
		$j_category = J_Category::model()->findAll();
		$this->render('//site/company/login', array(
				'j_category' => $j_category
			));
	}

	public function actionPost()
	{
		$j_category = J_Category::model()->findAll();
		$this->render('//site/company/post', array(
				'j_category' => $j_category
			));
	}

	
	public function actionPostadd(){
		if(!empty($_POST)){
			foreach($_POST as $key => $value){
					$answer[$key] = $value;
				}
			$connection = Yii::app()->db;
			$connection->active = true;

			$sql = "SELECT max(id_jobs) FROM jobs";
			$command = $connection->createCommand($sql);
			$dataReader = $command->query();
			foreach($dataReader as $row){}
			$connection->active = false;
			$tmpID = substr($row['max(id_jobs)'], 1);
			$tmpID++;
			if($tmpID<=10){
				$NewID = "j".str_pad($tmpID, 5, "0", STR_PAD_LEFT);
			}else{
				$NewID = "j".str_pad($tmpID, 5, "0", STR_PAD_LEFT);
			}

				$jobs = new Jobs();
				$jobs->id_jobs = $NewID;
				$jobs->post_date = new CDbExpression('NOW()');
				$jobs->position = $answer['job_position'];
				$jobs->job_description = $answer['job_detail'];
				$jobs->job_qualification = $answer['job_qualification'];
				$jobs->receive_rate = $answer['job_receive'];
				$jobs->salary_rate = $answer['job_salary'];
				$jobs->job_location = $answer['job_location'];
				$jobs->status = '0';
				$jobs->categoryid = $answer['selectCateJob'];
				$jobs->companyid = $answer['hidden_com_id'];
				$jobs->save();
			echo "ok";
		}else{
			echo "hello wo";
		}
	}
	public function actionProfile(){
		$companyid = Yii::app()->session['id_company'];
		$company = Company::model()->findByPk($companyid);
		$this->render('//site/company/profile', array(
				'company' => $company
			));
	}
	public function actionInterest()
	{
		$companyid = Yii::app()->session['id_company'];
		$criteria = new CDbCriteria();
			$criteria->condition = "
				companyid = :u 
			";
			$criteria->params = array(
				'u' => $companyid
			);

		$jobs = Jobs::model()->findAll($criteria);
		foreach($jobs as $row){
			$jid = $row->id_jobs;
				$criteria->condition = "
					jobid = :u 
				";
				$criteria->params = array(
					'u' => $jid
				);
				$n=Interest::model()->count($criteria);
			$count[] = $n;
		}
		$this->render('//site/company/interest', array(
				'jobs' => $jobs,
				'numjob' => $count
			));

	}
	public function actionViewresume(){
		if(!empty($_GET)){
			$id = $_GET['id'];
			$criteria = new CDbCriteria();
			$criteria->condition = "
					jobid = :u
			";
			$criteria->params = array(
					'u' => $id
			);
			$Job_interest = Interest::model()->findAll($criteria);
			$this->render('//site/company/viewresume', array(
					'jobinterest' => $Job_interest
				));
		}
	}

	public function actionSearchemployee(){
		$this->render('//site/company/searchemployee');
	}
	public function actionQuicksearch(){
		if(!empty($_POST)){
			$keyword = $_REQUEST['name'];
			$criteria = new CDbCriteria;
			$criteria->addSearchCondition('name_job',$keyword);
			$job_request = Job_Request::model()->findAll($criteria);
			
			echo '<table class="table">';
				echo '<thead>';
				echo '<tr>';
				echo '<th>ชื่อ</th>';
				echo '<th>ตำแหน่งที่ต้องการ</th>';
				echo '</tr>';
				echo '</thead>';
				echo '<tbody>';
				
			 foreach($job_request as $rowjob_request):
			 	if($rowjob_request->m_member->resume_status=='enable'){
				echo '<tr>';
				echo '<td>'.$rowjob_request->m_member->name.'</td>';
				echo '<td>'.$rowjob_request->name_job.'</td>';
				echo '</tr>';
			}
	 		endforeach;
			echo '</tbody>';
			echo '</table>';
		//echo "ok";
		}
		
	}
	public function actionChangejobstatus(){
		if(!empty($_GET)){
			$i = $_GET['i'];
			$jobid = $_GET['job'];
			$job = Jobs::model()->findByPk($jobid);
			$job->status = $i;
			$job->save();
			$this->redirect('interest');
		}
	}
	public function actionRegister()
	{

		$j_category = J_Category::model()->findAll();
		$this->render('//site/company/register', array(
				'j_category' => $j_category
			));
	}

	public function actionCheckuser(){
		$txtemail = $_REQUEST['email'];
		$criteria = new CDbCriteria();
			$criteria->condition = "
				c_email = :u 
			";
			$criteria->params = array(
				'u' => $txtemail
			);
			$checkuser = Company::model()->find($criteria);
			if(empty($checkuser)){
				echo "ok";
			}else{
				echo "no";
			}
	}

	public function actionChecklogin(){

		$txtemail = $_REQUEST['txtusername'];
		$txtpass = $_REQUEST['txtpassword'];

		$criteria = new CDbCriteria();
			$criteria->condition = "
				c_email = :u AND
				c_password = :p
			";
			$criteria->params = array(
				'u' => $txtemail,
				'p' => $txtpass
			);
			$checkuser = Company::model()->find($criteria);

			if(!empty($checkuser)){
				Yii::app()->session['id_company'] = $checkuser->id_company;
				Yii::app()->session['c_email'] = $checkuser->c_email;
				Yii::app()->session['c_name'] = $checkuser->c_name;
				echo "ok";
				//echo CJSON::encode($checkuser);
			}else{
				//echo CJSON::encode(array(
				//		"message" => "Username invalid"
				//	));
			}

	}

	public function actionLogout(){
		Yii::app()->session->destroy();
		$this->redirect('login');
	}

	public function actionRegistersave(){
		if(!empty($_POST)){
			foreach($_POST as $key => $value){
					$answer[$key] = $value;
				}
			$connection = Yii::app()->db;
			$connection->active = true;

			$sql = "SELECT max(id_company) FROM company";
			$command = $connection->createCommand($sql);
			$dataReader = $command->query();
			foreach($dataReader as $row){}
			$connection->active = false;
			$tmpID = substr($row['max(id_company)'], 2);
			$tmpID++;
			if($tmpID<=10){
				$NewID = "CP".str_pad($tmpID, 4, "0", STR_PAD_LEFT);
			}else{
				$NewID = "CP".str_pad($tmpID, 4, "0", STR_PAD_LEFT);
			}
				$company = new Company();
				$company->id_company = $NewID;
				$company->register_date = new CDbExpression('NOW()');
				$company->c_email = $answer['txtemail'];
				$company->c_password = $answer['txtpassword'];
				$company->c_name = $answer['txtname'];
				$company->c_address = $answer['txtaddress'];
				$company->c_tel = $answer['txttel'];
				$company->save();
			echo "ok";
		}
	}
	
}