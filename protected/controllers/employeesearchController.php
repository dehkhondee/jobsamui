<?php

class employeesearchController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$j_category = J_Category::model()->findAll();
		$article = Article::model()->findAll();
		
		$criteria = new CDbCriteria();
		$criteria->condition = "
					resume_status = :u 
				";
				$criteria->params = array(
					'u' => 'enable'
				);
		$employee = Members::model()->findAll($criteria);

		foreach($employee as $rowemp){
			$memberid = $rowemp->id_member;
			//echo $memberid;

				$criteria->condition = "
					memberid = :u 
				";
				$criteria->params = array(
					'u' => $memberid
				);
				$JR=Job_Request::model()->findAll($criteria);
				//echo print_r($JR);
				if($JR!=null){
					foreach($JR as $jrrow){
						$member[$memberid][] = $jrrow->name_job;
						
					}
				}
				//echo $member[$memberid][0];
		}

		$this->render('//site/employee_search', array(
				'j_category' => $j_category,
				'article' => $article,
				'employee' => $employee,
				'namejob' => $member
			));

	}

	

}