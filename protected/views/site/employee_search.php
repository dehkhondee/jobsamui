<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8"/>
	<title>JOB Samui</title>
	
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="css/job-style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 
</head>
<body>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a style="padding: 10px 15px;" >NEW USER?:</a></li>
					<li><a style="padding: 10px 15px;" href="#">SIGN UP</a></li>
					<li><a style="padding: 10px 15px;" href="#">SIGN IN</a></li>
					<li><a style="padding: 10px 15px;" href="#">HELP</a></li>
				</ul>
			</div>
	</div>
</nav>
<div class="container-min">
	<div class="row">
		<div class="col-md-12 jobheader">
			<div class="pull-left"><img src="images/job-logo.png"></div>
			<div class="pull-right" style="text-align:right;"><img src="images/ad_top.png"></div>
		</div>
		<!-- col-md-12 -->
	</div>
	<!-- row header -->
	<div class="row" style="padding:0px;">
			<nav class="nav navbar-default job-navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>

				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav" style="font-weight:bold;">
						<li><a href="index.php">HOME</a></li>
						<li><a href="jobsearch">Job Search</a></li>
						<li><a href="employeesearch">Employee Search</a></li>
						<li><a href="#">HELP</a></li>
					</ul>
				
					<div class="pull-right">
						<img src="images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
					</div>
				</div>
			</nav>
	</div>
	<!-- row menu header -->
	<div class="row" style="margin-top:15px;">
		<div class="col-md-3 job-leftbar">
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<div class="job-box-header">
						<span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#ff9900;">JOB SAMUI:</span><br>
						<span style="margin-top:-10px;font-size:14px;color:#ffffff;">QUICK JOB SEARCH</span>
					</div>
					<!-- job-box-header -->
					<div class="job-box-content">
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Enter Keyword(s)</span>
								<div class="form-group">
									<input name="txtkeyword" class="form-control input-sm job-input-search" type="text"/>
								</div>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Select a Job Category</span>
								<select name="txtselect" class="form-control input-sm job-input-search">
									<option value="">--</option>
									<?php foreach($j_category as $row_category):?>
										<option value="<?php echo $row_category->id_category;?>"><?php echo $row_category->category_name;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12" style="text-align:right;">
								<button class="btn btn-warning" style="font-weight:bold;margin-top:20px;">Search Job</button>
								<br>
								<span style="font-family:'TH SarabunPSK';font-size:18px;font-weight:bold;">ค้นหา: งานด่วน</span>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
					</div>
					<!-- job-box-content -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
					<?php foreach($article as $rowarticle): ?>
					<table style="border-bottom: 1px solid #dfdfdf;">
						<tr>
							<td><span style="font-size:20px;"><?php echo $rowarticle->atc_post_subject;?></span></td>
						</tr>
						<tr>
							<td><span style="font-size:14px;"><?php echo $rowarticle->atc_post_detail;?></span></td>
						</tr>
					</table>
				<?php endforeach;?>
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="padding:10px;border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
					<img src="images/250-150-ads.png" style="width:100%;">
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-3 job-leftbar-->
		<div class="col-md-9" style="padding: 0px;margin-top:20px;">
			<div class="row">
				<div class="col-md-12">
					<img src="images/for-member.png" style="float:right;">	
					<img src="images/job-search-job.png">
					<hr style="border-width:3px;" >
					
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="text-align:center;">
					<table class="table" style="border-bottom: 1px solid #dfdfdf;">
						<?php //$row = getAllJobs();
							foreach($employee as $result):?>
						<tr>
							<td width="60%">
								<table width="100%">
									<tr>
										<td height="120" valign="top">
											<i class="glyphicon glyphicon-bookmark" style="font-size:18px;"></i>
											<a href="jobs_details.php" style="font-size:18px;font-weight:bold;text-decoration:none;">
											<?php $memberid=$result->id_member;
												
												for($i=0;$i<=count($namejob[$memberid]);$i++){
													echo $namejob[$memberid][$i].'/';
												}
												?>
											
											</a>
											<br>
											<span>
												สาขาอาชีพ : <?php echo $result->mj_category->category_name;?>
											</span>
											<br>
											<span>
												เพศ : <?php echo $result->my_qualification->gender;?>
											</span>
											<br>
											<span>
												เงินเดือนที่ต้องการ : <?php echo $result->my_request->salary;?>
											</span>
											<br>
											<span>
												
											</span>
										</td>
									</tr>
								</table>
							</td>
							<td align="center" valign="top">
								<img src="images/profile.jpg" style="height:100px;">
							</td>
						</tr>
						
						<?php endforeach; ?>
					</table>
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-9 -->
	</div>
	<!-- row -->
</div>
<!-- container -->	
<div class="container" style="margin-top:20px;">
	 <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
        <!-- Footer -->
</div>
<!-- container -->
</body>
</html>