<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8"/>
	<title>JOB Samui</title>
	
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="css/job-style.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
 
</head>
<body>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a style="padding: 10px 15px;" >NEW USER?:</a></li>
					<li><a style="padding: 10px 15px;" href="#">SIGN UP</a></li>
					<li><a style="padding: 10px 15px;" href="#">SIGN IN</a></li>
					<li><a style="padding: 10px 15px;" href="#">HELP</a></li>
				</ul>
			</div>
	</div>
</nav>
<div class="container-min">
	<div class="row">
		<div class="col-md-12 jobheader">
			<div class="pull-left"><img src="images/job-logo.png"></div>
			<div class="pull-right" style="text-align:right;"><img src="images/ad_top.png"></div>
		</div>
		<!-- col-md-12 -->
	</div>
	<!-- row header -->
	<div class="row" style="padding:0px;">
			<nav class="nav navbar-default job-navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>

				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav" style="font-weight:bold;">
						<li><a href="index.php">HOME</a></li>
						<li><a href="jobsearch">Job Search</a></li>
						<li><a href="">Employee Search</a></li>
						<li><a href="#">HELP</a></li>
					</ul>
				
					<div class="pull-right">
						<img src="images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
					</div>
				</div>
			</nav>
	</div>
	<!-- row menu header -->
	<div class="row" style="margin-top:15px;">
		<div class="col-md-3 job-leftbar">
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<div class="job-box-header">
						<span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#ff9900;">JOB SAMUI:</span><br>
						<span style="margin-top:-10px;font-size:14px;color:#ffffff;">QUICK JOB SEARCH</span>
					</div>
					<!-- job-box-header -->
					<div class="job-box-content">
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Enter Keyword(s)</span>
								<div class="form-group">
									<input name="txtkeyword" class="form-control input-sm job-input-search" type="text"/>
								</div>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Select a Job Category</span>
								<select name="txtselect" class="form-control input-sm job-input-search">
									<option value="">--</option>
									<?php foreach($j_category as $row_category):?>
										<option value="<?php echo $row_category->id_category;?>"><?php echo $row_category->category_name;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12" style="text-align:right;">
								<button class="btn btn-warning" style="font-weight:bold;margin-top:20px;">Search Job</button>
								<br>
								<span style="font-family:'TH SarabunPSK';font-size:18px;font-weight:bold;">ค้นหา: งานด่วน</span>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
					</div>
					<!-- job-box-content -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
					<?php foreach($article as $rowarticle): ?>
					<table style="border-bottom: 1px solid #dfdfdf;">
						<tr>
							<td><span style="font-size:20px;"><?php echo $rowarticle->atc_post_subject;?></span></td>
						</tr>
						<tr>
							<td><span style="font-size:14px;"><?php echo $rowarticle->atc_post_detail;?></span></td>
						</tr>
					</table>
				<?php endforeach;?>
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="padding:10px;border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
					<img src="images/250-150-ads.png" style="width:100%;">
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-3 job-leftbar-->
		<div class="col-md-9" style="padding: 0px;margin-top:20px;">
			<div class="row">
				<div class="col-md-12">
					<img src="images/for-member.png" style="float:right;">
					<img src="images/job_detail2.png">
					<br><?php //$rowjob = CJSON::decode($job);
							 ?>
					<hr style="border-width:3px;" >
					<ol class="breadcrumb">
					    <li><a href="index.php" title="Job Samui" style=" text-decoration: none;">Home</a></li>
					    <li><a href="jobsearch" title="Job samui Search" style=" text-decoration: none;">Jobs Search</a></li>
					    <li class="active" title="<?php echo $job->position;?> <?php echo $job->job_location;?>">
					  	    <strong><?php echo $job->position;?></strong>
					    </li>
					</ol>
					<div class="row">
						<div class="col-md-12" >
							<div style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;padding:10px;">
								<div class="row">
									<div class="col-md-7">
										<span style="color:#ff9900;font-weight:bold;font-size:20px;"><?php echo $job->my_company->c_name;?></span><br>
										<span>
											<?php echo $job->my_company->c_detail;?>
										</span>
									</div>
									<div class="col-md-4">
										<img src="images/profile.jpg">
									</div>
								</div>
								<!-- row -->
								<br><br>
								<span class="label label-warning" style="font-size:18px;padding:3px 20px;">Position</span>
								<strong><?php echo $job->position;?></strong>
								<br><br>
								<span  class="label label-warning">Job Description</span><br><br>
								<span><?php echo $job->job_description;?></span>
								<br><br>
								<span  class="label label-warning">Job Qualification</span><br>
								<span><?php echo $job->job_qualification;?></span>
								<br><br>
								<span  class="label label-warning">Salary</span>
								<span><?php echo $job->salary_rate;?></span>
								<span class="label label-warning" style="margin-left:20px;">Apply</span>
								<span><?php echo $job->receive_rate;?></span>
								<br><br>
								<span  class="label label-warning">Location</span><br>
								<span><?php echo $job->job_location;?></span>
							</div>
						</div>
						<!-- col-md-12 -->
					</div>
					<!-- row -->
					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-warning">Apply Now</button>
						</div>
						<!-- col-md-12 -->
					</div>
					<!-- row -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-9 -->
	</div>
	<!-- row -->
</div>
<!-- container -->
<div class="container" style="margin-top:20px;">
	 <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
        <!-- Footer -->
</div>
<!-- container -->
</body>
</html>