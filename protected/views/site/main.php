<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="css/job-style.css">
	<link rel="stylesheet" type="text/css" href="css/jssor.css">
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<script src="assets/js/jquery-2.1.1.min.js"></script>
  	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="assets/js/jssor.core.js"></script>
    <script type="text/javascript" src="assets/js/jssor.utils.js"></script>
    <script type="text/javascript" src="assets/js/jssor.slider.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            var options = {
                $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                $AutoPlaySteps: 4,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, default value is 1

                $ArrowKeyNavigation: true,   			            //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                $SlideDuration: 300,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                $SlideWidth: 200,                                   //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                //$SlideHeight: 150,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                $SlideSpacing: 3, 					                //[Optional] Space between each slide in pixels, default value is 0
                $DisplayPieces: 3,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                $ParkingPosition: 0,                              //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                $PlayOrientation: 5,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                    $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                    $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                    $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                    $SpacingX: 0,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                    $SpacingY: 0,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                    $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                },

                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                    $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                    $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                    $Steps: 4                                       //[Optional] Steps to go for each navigation request, default value is 1
                }
            };

            var jssor_slider1 = new $JssorSlider$("slider1_container", options);

            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizes
            function ScaleSlider() {
                var bodyWidth = document.body.clientWidth;
                if (bodyWidth)
                    jssor_slider1.$SetScaleWidth(Math.min(bodyWidth, 809));
                else
                    window.setTimeout(ScaleSlider, 30);
            }

            ScaleSlider();

            if (!navigator.userAgent.match(/(iPhone|iPod|iPad|BlackBerry|IEMobile)/)) {
                $(window).bind('resize', ScaleSlider);
            }


            //if (navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
            //    $(window).bind("orientationchange", ScaleSlider);
            //}
            //responsive code end
        });
    </script>
</head>
<body>
<?php
	if(Yii::app()->session['id_member']!=''){ ?>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container-min">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
				          <a style="padding: 10px 15px;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo Yii::app()->baseUrl;?>/images/discovered.png" style="width:20px;"> <?php echo Yii::app()->session['email'];?> <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="#">ค้นหางาน</a></li>
				            <li><a href="resume">แก้ไขประวัติ</a></li>
				            <li><a href="#">งานที่สมัคร</a></li>
				            <li><a href="#">บริษัทที่ดูประวัติ</a></li>
				            <li><a href="#">ตั้งค่าบัญชีผู้ใช้งาน</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="logout.php">ออกจากระบบ</a></li>
				          </ul>
				    </li>
				</ul>
			</div>
	</div>
</nav>
<!-- top menu bar -->
<?php	}elseif(Yii::app()->session['id_company']!=''){ ?>
	<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container-min">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
				          <a style="padding: 10px 15px;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo Yii::app()->baseUrl;?>/images/employee.png" style="width:20px;"> <?php echo $_SESSION['c_email'];?> <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="company_emp_search.php">ค้นหาพนักงาน</a></li>
				            <li><a href="post">ลงประกาศงาน</a></li>
				            <li><a href="#">สถานะตำแหน่งงาน</a></li>
				            <li><a href="#">ตั้งค่าบัญชีผู้ใช้งาน</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="logout.php">ออกจากระบบ</a></li>
				          </ul>
				    </li>
				</ul>
			</div>
	</div>
</nav>
<!-- top menu bar -->
<?php	}else{ ?>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container-min">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a style="padding: 10px 15px;" >NEW USER?:</a></li>
					<li><a style="padding: 10px 15px;" href="#">SIGN UP</a></li>
					<li><a style="padding: 10px 15px;" href="#">SIGN IN</a></li>
					<li><a style="padding: 10px 15px;" href="#">HELP</a></li>
				</ul>
			</div>
	</div>
</nav>
<?php }
?>

<div class="container-min">
	<div class="row">
		<div class="col-md-12 jobheader">
			<div class="pull-left"><img src="images/job-logo.png"></div>
			<div class="pull-right" style="text-align:right;"><img src="images/ad_top.png"></div>
		</div>
		<!-- col-md-12 -->
	</div>
	<!-- row header -->
	<div class="row" style="padding:0px;">
			<nav class="nav navbar-default job-navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>

				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav" style="font-weight:bold;">
						<li><a href="index">HOME</a></li>
						<li><a href="jobsearch">Jobs Search</a></li>
						<li><a href="employeesearch">Employee Search</a></li>
						<li><a href="#">News</a></li>
						<li><a href="#">Advertising</a></li>
					</ul>
				
					<div class="pull-right">
						<img src="images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
					</div>
				</div>
			</nav>
	</div>
	<!-- row menu header -->
	<div class="row" style="margin-top:15px;">
		<div class="col-md-3 job-leftbar">
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<div class="job-box-header">
						<span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#ff9900;">JOB SAMUI:</span><br>
						<span style="margin-top:-10px;font-size:14px;color:#ffffff;">QUICK JOB SEARCH</span>
					</div>
					<!-- job-box-header -->
					<div class="job-box-content">
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Enter Keyword(s)</span>
								<div class="form-group">
									<input name="txtkeyword" class="form-control input-sm job-input-search" type="text"/>
								</div>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Select a Job Category</span>
								<select name="txtselect" class="form-control input-sm job-input-search">
									<option value="">--</option>
									<?php foreach($j_category as $row_category):?>
										<option value="<?php echo $row_category->id_category;?>"><?php echo $row_category->category_name;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12" style="text-align:right;">
								<button class="btn btn-warning" style="font-weight:bold;margin-top:20px;">Search Job</button>
								<br>
								<span style="font-family:'TH SarabunPSK';font-size:18px;font-weight:bold;">ค้นหา: งานด่วน</span>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
					</div>
					<!-- job-box-content -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="hidden-xs col-md-10" style="border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
					<?php foreach($article as $rowarticle): ?>
					<table style="border-bottom: 1px solid #dfdfdf;">
						<tr>
							<td><span style="font-size:20px;"><?php echo $rowarticle->atc_post_subject;?></span></td>
						</tr>
						<tr>
							<td><span style="font-size:14px;"><?php echo $rowarticle->atc_post_detail;?></span></td>
						</tr>
					</table>
				<?php endforeach;?>
				</div>
				<!-- col-md-10-->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="padding:10px;border: 1px solid #dfdfdf;border-radius:4px;margin:15px;">
					<img src="images/250-150-ads.png" style="width:100%;">
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-3 job-leftbar -->
		<div class="col-md-9" style="padding: 0px;">
			<div class="row">
				<div class="col-md-6" style="padding:0px;padding-left:15px;">
					<div class="row">
						<div class="col-md-12" style="text-align:right;">
							<img src="images/for-business.png">
						</div>
					</div>
					<!-- row -->
					<div class="job-box-header">
						<img src="images/employee.png" style="float:right; margin-right:20px;margin-top:15px;">
						<span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#41d6fa;">JOB SAMUI:</span><br>
						<span style="margin-top:-10px;font-size:14px;color:#ffffff;">EMPLOYEE AREA</span>
					</div>
					<!-- job-box-header -->
					<div class="job-box-content">
						<div class="row">
							<div class="col-md-5">
								<span style="font-size:30px;">Upload</span><br>
								<span style="font-size:12px;font-weight:bold;">or build your business</span>
								
							</div>
							<!-- col-md-6 -->
							<div class="col-md-7" style="text-align:right;margin-top:20px;">
								<button  class="btn btn-default job-btn-post-business" style="padding:5px 25px;">
									Post Your business
								</button>
							</div>
							<!-- col-md-6 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12" style="margin-top:5px;font-family:'TH SarabunPSK';font-size:20px;font-weight:bold;">
								สมัครสมาชิกใหม่?: 
									<a href="company/register" style="padding:0px 5px;text-decoration:none;">สมัครสมาชิก</a>
									|<a href="company/login" style="padding:0px 5px;text-decoration:none;">เข้าสู่ระบบ</a>
									|<a style="padding:0px 5px;text-decoration:none;">ช่วยเหลือ</a>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<span  style="color:#949494;font-weight:bold;font-size:12px;">HAVING AN UPDATED AND SEARCHABLE RESUME IS THE BEST WAY FOR EMPLOYERS TO FIND YOU.</span>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
					</div>
					<!-- job-box-content -->
				</div>
				<!-- col-md-6 -->
				<div class="col-md-6" style="padding:0px;padding-right:15px;padding-left:5px;">
					<div class="row">
						<div class="col-md-12" style="text-align:right;">
							<img src="images/for-member.png">
						</div>
					</div>
					<!-- row -->
					<div class="job-box-header">
						<img src="images/discovered.png" style="float:right; margin-right:20px;margin-top:3px;">
						<span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#ff9900;">JOB SAMUI:</span><br>
						<span style="margin-top:-10px;font-size:14px;color:#ffffff;">BE DISCOVERED</span>
					</div>
					<!-- job-box-header -->
					<div class="job-box-content">
						<div class="row">
							<div class="col-md-5">
								<span style="font-size:30px;">Upload</span><br>
								<span style="font-size:12px;font-weight:bold;">or build your Resume</span>
								
							</div>
							<!-- col-md-6 -->
							<div class="col-md-7" style="text-align:right;margin-top:20px;">
								<button  class="btn btn-default job-btn-post-resume" style="padding:5px 25px;">
									Post Your Resume
								</button>
							</div>
							<!-- col-md-6 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12" style="margin-top:5px;font-family:'TH SarabunPSK';font-size:20px;font-weight:bold;">
								สมัครสมาชิกใหม่?: 
									<a href="member/register" style="padding:0px 5px;text-decoration:none;">สมัครสมาชิก</a>
									|<a href="member/login" style="padding:0px 5px;text-decoration:none;">เข้าสู่ระบบ</a>
									|<a style="padding:0px 5px;text-decoration:none;">ช่วยเหลือ</a>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<span  style="color:#949494;font-weight:bold;font-size:12px;">HAVING AN UPDATED AND SEARCHABLE RESUME IS THE BEST WAY FOR EMPLOYERS TO FIND YOU.</span>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						
					</div>
					<!-- job-box-content-->
				</div>
				<!-- col-md-4 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="height:auto;margin-top:20px;padding:25px;">
					<img src="images/hotjob.png">
					<hr style="border-width:3px;" />
					<div class="row">
						<div class="col-md-12">
							 <div id="slider1_container" style="position: relative; top: 0px; left: 0px; width: 809px; height: 150px; overflow: hidden;">

						        <!-- Loading Screen -->
						        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
						            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
						                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
						            </div>
						            <div style="position: absolute; display: block; background: url(images/loading.gif) no-repeat center center;
						                top: 0px; left: 0px;width: 100%;height:100%;">
						            </div>
						        </div>

						        <!-- Slides Container -->
						        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 809px; height: 150px; overflow: hidden;">
						            <div><a href="#"><img u="image" src="images/ancient-lady/005.jpg" /></a></div>
						            <div><img u="image" src="images/ancient-lady/006.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/011.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/013.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/014.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/019.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/020.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/021.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/022.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/024.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/025.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/027.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/029.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/030.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/031.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/032.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/034.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/038.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/039.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/043.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/044.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/047.jpg" /></div>
						            <div><img u="image" src="images/ancient-lady/050.jpg" /></div>
						        </div>
						        
						        <!-- bullet navigator container -->
						        <div u="navigator" class="jssorb03" style="position: absolute; bottom: 4px; right: 6px;">
						            <!-- bullet navigator item prototype -->
						            <div u="prototype" style="position: absolute; width: 21px; height: 21px; text-align:center; line-height:21px; color:white; font-size:12px;"><NumberTemplate></NumberTemplate></div>
						        </div>
						        <!-- Bullet Navigator Skin End -->
        
						    </div>
						    <!-- Jssor Slider End -->
						</div>
						<!-- col-md-12 -->
					</div>
					<!-- row -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="height:auto;margin-top:10px;">
					<span>ตำแหน่งงานใหม่ประจำวัน <strong>1</strong> ตำแหน่ง</span>
					<div class="CSSTableGenerator">
						<table>
							<tr>
								<th width="50%">Position</th>
								<th width="30%">Company</th>
								<th width="20%">Location</th>
							</tr>
							<?php foreach($jobs as $rowjobs):?>
								<tr>
									<td height="40" style=""><a href="jobs?id=<?php echo $rowjobs->id_jobs;?>"><?php echo $rowjobs->position;?></a></td>
									<td><?php echo $rowjobs->my_company->c_name;?></td>
									<td>PathumThani</td>
								</tr>
							<?php endforeach;?>
							<tr>
								<td height="40" style=""><a href="#">Accounting Manager</a></td>
								<td>Execusive search service Co.,Ltd.</td>
								<td>PathumThani</td>
							</tr>

						</table>
					</div>
					<!-- CSSTableGenerator -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<img src="images/job-search.png">
					<hr style="border-width:2px;" />
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="height:auto;margin-top:10px;">
					<div class="row">
						<div class="col-md-6" style="text-align:center;">
							<img src="images/map-samui.png">
						</div>
						<!-- col-md-6 -->
						<div class="col-md-6" style="text-align:center;">
							<img src="images/reating.png"><br>
							<img src="images/reating-2.png">
							<a href="http://www.samuinavigator.com" target="_blank"><img src="images/navigator.png"></a>
						</div>
						<!-- col-md-6 -->
					</div>
					<!-- row -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<img src="images/job-category.png">
					<hr style="border-width:2px;">
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-12" style="height:auto;margin-top:20px;">
					<table width="100%" >
						<tr style="border-bottom: 1px solid #dfdfdf;">
							<td>fdsfvdsfds</td>
						</tr>
						<tr style="border-bottom: 1px solid #dfdfdf;">
							<td>fdsfvdsfds</td>
						</tr>
					</table>
				</div>
				<!-- col-md-11 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-9 -->
	</div>
	<!-- row -->
</div>
<!-- container -->

<div class="container-min" style="margin-top:20px;">
	 <hr>
        <footer class="section section-warning">
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
        <!-- Footer -->
</div>
<!-- container -->
</body>
</html>