<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8"/>
	
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/job-style.css">
	<title>JOB Samui</title>
	<script src="<?php echo Yii::app()->baseUrl;?>/assets/js/jquery-2.1.1.min.js"></script>
  	<script src="<?php echo Yii::app()->baseUrl;?>/assets/bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/js/jquery.form.min.js"></script>
 	<script>
 		function checkuser(){
 			var email = $('input[name=txtemail]').val();
 			$.ajax({
 				url: 'checkuser',
 				type: 'post',
 				dataType: 'html',
 				data:{
 					email: email
 				},
 				success:function(data){
 					if(data == "ok"){
 						var alert = '<font color="green"><i class="glyphicon glyphicon-ok"></i> อีเมล์ดังกล่าวสามารถใช้งานได้</font>';
 						$('#textalert').html(alert);
 					}else{
 						var alert = '<font color="red"><i class="glyphicon glyphicon-remove"></i> อีเมล์ดังกล่าวไม่สามารถใช้งานได้</font>';
 						$('#textalert').html(alert);
 						$('input[name=txtemail]').focus();
 					}
 				}
 			});
 		}
 		$(function() {
                $('#frmRegister').ajaxForm({
                        url: 'registersave',
                        type: 'post',
                        dataType: 'html', 
                    beforeSend: function(){
                        //$('#pg').val(0);  //กำหนดค่า progress ให้เป็น 0
                        //$('#pc').html('0%');  //ให้แสดงค่า 0%
                        $('#btnsend').html('<img src="../images/ajax-loader-wi.gif"> loading..'); 

                    },
                    uploadProgress: function(event, position, total, percent){
                        //$('#pg').val(percent);
                        //$('#pc').html(percent + '%');
                         $('#btnsend').html('<img src="../images/ajax-loader-wi.gif"> loading..'); 
                    },
                    success: function(result){
                        //$('#pg').val(100);
                       // $('#pc').html('100%');
                                    
                       // $('#result').html(result);
                        $('#frmRegister')[0].reset();
                        $('#btnsend').html('ลงทะเบียน|register');
                        if(result == "ok"){
                        	var alert = '<div class="alert alert-success"><strong>Success!</strong> สมัครสมาชิกเสร็จสิ้น กรุณาเข้าสู่ระบบ <a href="login">คลิก</a>.</div>';
                        	$('#alertregister').html(alert);
                        	//window.location='member_login.php';
                        }else{
                        	var alert = '<div class="alert alert-danger"><strong>Danger!</strong> email and password not founds.</div>';
                        	$('#alertregister').html(alert);
                        	//$('input[name=txtusername]').focus();
                        }
                       
                        //var reid = $('input[name=academichiddenID]').val()
                       // editAcademic(reid);
                       
                       //window.location='resume.php';
                    },
                    error: function(xhr, textStatus){
                        alert(textStatus);
                    }
                });
            }); 
		function checkvalue(){
			var username = $('input[name=txtusername]').val();
			var password = $('input[name=txtpassword]').val();
			var confirm = $('input[name=txtconfirm]').val();
			if(username == ""){
				var alert = '<div class="alert alert-warning"><strong>Warning!</strong> ยังไม่ได้ป้อน email!!</div>';
                $('#alertregister').html(alert);
                return false;
			}else if(password == ""){
				var alert = '<div class="alert alert-warning"><strong>Warning!</strong> ยังไม่ได้ป้อน password!!</div>';
                $('#alertregister').html(alert);
                return false;
			}else if(confirm == ""){
				var alert = '<div class="alert alert-warning"><strong>Warning!</strong> ยังไม่ได้ยืนยัน password!!</div>';
                $('#alertregister').html(alert);
                return false;
			}

		}
 	</script>
</head>
<body>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
    <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a style="padding: 10px 15px;" >NEW USER?:</a></li>
                    <li><a style="padding: 10px 15px;" href="#">SIGN UP</a></li>
                    <li><a style="padding: 10px 15px;" href="#">SIGN IN</a></li>
                    <li><a style="padding: 10px 15px;" href="#">HELP</a></li>
                </ul>
            </div>
    </div>
</nav>
<div class="container-min">
	<div class="row">
		<div class="col-md-12 jobheader">
			<div class="pull-left"><img src="../images/job-logo.png"></div>
			<div class="pull-right" style="text-align:right;"><img src="../images/ad_top.png"></div>
		</div>
		<!-- col-md-12 -->
	</div>
	<!-- row header -->
	<div class="row" style="padding:0px;">
			<nav class="nav navbar-default job-navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>

				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav" style="font-weight:bold;">
						<li><a href="../index.php">HOME</a></li>
						<li><a href="../jobsearch">Job Search</a></li>
						<li><a href="#">Employee Search</a></li>
						<li><a href="#">HELP</a></li>
					</ul>
				
					<div class="pull-right">
						<img src="../images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
					</div>
				</div>
			</nav>
	</div>
	<!-- row menu header -->
	<div class="row" style="margin-top:15px;">
		<div class="col-md-3 job-leftbar">
			<div class="row">
				<div class="col-md-12" style="margin-top:20px;">
					<div class="job-box-header">
						<span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#ff9900;">JOB SAMUI:</span><br>
						<span style="margin-top:-10px;font-size:14px;color:#ffffff;">QUICK JOB SEARCH</span>
					</div>
					<!-- job-box-header -->
					<div class="job-box-content">
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Enter Keyword(s)</span>
								<div class="form-group">
									<input name="txtkeyword" class="form-control input-sm job-input-search" type="text"/>
								</div>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12">
								<span style="font-weight:bold;">Select a Job Category</span>
								<select name="txtselect" class="form-control input-sm job-input-search">
									<option value="">--</option>
									<?php foreach($j_category as $row_category):?>
										<option value="<?php echo $row_category->id_category;?>"><?php echo $row_category->category_name;?></option>
									<?php endforeach;?>
								</select>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
						<div class="row">
							<div class="col-md-12" style="text-align:right;">
								<button class="btn btn-warning" style="font-weight:bold;margin-top:20px;">Search Job</button>
								<br>
								<span style="font-family:'TH SarabunPSK';font-size:18px;font-weight:bold;">ค้นหา: งานด่วน</span>
							</div>
							<!-- col-md-12 -->
						</div>
						<!-- row -->
					</div>
					<!-- job-box-content -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
					
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
			<div class="row">
				<div class="col-md-10" style="padding:10px;border: 1px solid #dfdfdf;border-radius:4px;margin:25px 20px;">
					<img src="../images/250-150-ads.png" style="width:100%;">
				</div>
				<!-- col-md-10 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-3 job-leftbar -->
		<div class="col-md-9" style="padding: 0px;">
			<div class="row">
				<div class="col-md-12">
					<div class="job-content">
					<img src="../images/for-member.png" style="float:right;">	
						<h1>Business Register</h1>		
						
						<hr style="border-width:3px;">
						<div id="alertregister"></div>
						<table class="contenttable table table-bordered" id="tableregister"> 
							<form class="form-inline" id="frmRegister">
								<tr>
									<td width="40%" height="50" colspan="2" style="font-weight:bold;">System Information (ข้อมูลเข้าใช้งานระบบ)</td>
								</tr>
								<tr>
									<td align="right">Email / Username <font color="red">* </font></td>
									<td>
										<input name="txtemail" class="form-control job-input" type="email" required onchange="return checkuser()" />
										<span id="textalert"></span><span id="alertuser"></span>
									</td>
								</tr>
								<tr>
									<td align="right" height="40">Password : <font color="red">* </font> </td>
									<td><input name="txtpassword" class="form-control job-input" type="password" required/>
									<span id="alertpass"></span>
									</td>
								</tr>
								<tr>
									<td align="right" height="40">Confirm Password : <font color="red">* </font> </td>
									<td>
										<input name="txtconfirm" class="form-control job-input" type="password" required/>
										<span id="alertconfirm"></span>
									</td>
								</tr>
								<tr>
									<td width="40%" height="50" colspan="2" style="font-weight:bold;">Business information (ข้อมูลธุรกิจ)</td>
								</tr>
								<tr>
									<td align="right" height="40">ชื่อบริษัท <font color="red">* </font></td>
									<td>
										<input name="txtname" class="form-control job-input" type="text" required/>
										<span id="alertname"></span>
									</td>
								</tr>
								<tr>
									<td align="right">รายละเอียดของบริษัท <font color="red">* </font></td>
									<td>
										<textarea name="txtdetail" class="form-control job-input"  required></textarea>
										<span id="alertdetail"></span>
									</td>
								</tr>
								<tr>
									<td align="right">Phone(เบอร์ติดต่อ) <font color="red">* </font></td>
									<td>
										<input name="txttel" class="form-control job-input" type="text" required/>
										<span id="alerttel"></span>
									</td>
								</tr>
								<tr>
									<td align="right">Security Code <font color="red">* </font></td>
									<td>
										<input name="txtcode" class="form-control job-input" type="text" style="width:70px;" required/>
										<img src="image_capcha.php" id="mycapt" align="absmiddle" />
										<span id="alertcode"></span>
									</td>
								</tr>
								<tr>
									<td colspan="2" align="center"><button class="btn btn-warning" style="font-weight:bold;"  id="btnsend" >ลงทะเบียน|Register</button>
										<input type="hidden" name="MAX_FILE_SIZE" value="1048576">
		                                                <progress id="pg" value="0" max="100" style="display:none;"></progress>
		                                                <span id="pc" style="display:none;">0%</span>
		                                                <div id="result"></div>
									</td>
								</tr>
							</form>
						</table>
						
					</div>
					<!-- job-box-content -->
				</div>
				<!-- col-md-12 -->
			</div>
			<!-- row -->
		</div>
		<!-- col-md-9 -->
	</div>
	<!-- row -->
</div>
<!-- container -->
<div class="container" style="margin-top:20px;">
	 <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
        <!-- Footer -->
</div>
<!-- container -->
</body>
</html>