<?php
	if(Yii::app()->session['id_company']==''){
		echo "<meta charset=\"utf-8\"/><script>alert('ยังไม่ได้เข้าสู่ระบบ');window.location='login';</script>";exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta charset="utf-8"/>
	<title>JOB Samui</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/mystyle.css">
  	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/job-style.css">
  	<!-- Add fancyBox main JS and CSS files -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/assets/fancy/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  	<script src="<?php echo Yii::app()->baseUrl;?>/assets/js/jquery.js"></script>
  	<script src="<?php echo Yii::app()->baseUrl;?>/assets/bootstrap/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/fancy/source/jquery.fancybox.js?v=2.1.5"></script>
  	<script src="<?php echo Yii::app()->baseUrl;?>/assets/js/jquery.form.min.js"></script>
  	<style type="text/css">
	  	.narbar-gradient{
	  		background-color: rgba(194, 188, 188, 1);
	    	background: -webkit-linear-gradient(top, rgba(214, 214, 214, 1) 0%, rgba(194, 188, 188, 1) 100%);
	    	background: linear-gradient(to bottom, rgba(214, 214, 214, 1) 0%, rgba(194, 188, 188, 1) 100%);
	    }
	    .div {
	    	text-align: center;
	    	display: block;
	    	width: :100%;
	    }
	    .div div label{
			display: inline-block;
			width: 250px;
			text-align: right;
			padding-right: 10px;
		} 
		.div div{
			padding-bottom: 10px;
		}
		.div div div{
			width: 400px;
			display: inline-block;
			text-align: left;
		}
		.job-table tr td{
			padding: 5px 10px;
		}
		.resume-header{
			text-align:center;
			font-size:24px;
			border-bottom-right-radius: 6px;
			border-bottom-left-radius: 6px;
			background-color:#41d6fa;
			display: inline-block;
			width: 250px;
			color:#ffffff;
			margin-bottom: 10px;
		}
  	</style>
  	<script>
  		 $(function(){
	        $('#frmPostJob').ajaxForm({
	        	url: 'postadd',
	        	type: 'post',
	        	dataType: 'html',
	        	beforeSend: function(){
					//$('#pg').val(0);  //กำหนดค่า progress ให้เป็น 0
		       	 	//$('#pc').html('0%');  //ให้แสดงค่า 0%
		       	 	var btn = '<button class="btn btn-info btn-sm" style="width:150px;" disabled><img src="../images/ajax-loader-wi.gif"/> Saving...</button>';
		       	 	$('#divbtn').html(btn);
		  	  	},
		  	  	uploadProgress: function(event, position, total, percent){
					//$('#pg').val(percent);
		       	 	//$('#pc').html(percent + '%');
		       	 	var btn = '<button class="btn btn-info btn-sm" style="width:150px;" disabled><img src="../images/ajax-loader-wi.gif"/> Saving...</button>';
		       	 	$('#divbtn').html(btn);
		    	},
		    	success: function(result){
					//$('#pg').val(100);
		        	//$('#pc').html('100%');
					//$.fancybox.close();
					//$('#result').html(result);
					var btn = '<button class="btn btn-info btn-sm" style="width:150px;"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>';
		       	 	$('#divbtn').html(btn);
					$('#frmPostJob')[0].reset();
					if(result=="ok"){
						window.location='interest';
					}
					//var psnid = $('input[name=psnid]').val();
					//parent.jQuery.fancybox.close();
					//parent.$('#imageprofile').html(result);
					//parent.editPersonnel(psnid);
					//window.location='job_post.php';
		    	},
				error: function(xhr, textStatus){
					alert(textStatus);
				}
	        });
	    });
  	</script>
</head>
<body>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
				          <a style="padding: 10px 15px;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="../images/employee.png" style="width:20px;"> <?php echo $_SESSION['c_email'];?> <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="company_emp_search.php">ค้นหาพนักงาน</a></li>
				            <li><a href="post">ลงประกาศงาน</a></li>
				            <li><a href="#">สถานะตำแหน่งงาน</a></li>
				            <li><a href="#">ตั้งค่าบัญชีผู้ใช้งาน</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="logout.php">ออกจากระบบ</a></li>
				          </ul>
				    </li>
				</ul>
			</div>
	</div>
</nav>
<!-- top menu bar -->
<div class="container-min">
	<div class="row jobheader">
			<div class="pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/job-logo.png"></div>
			<div class="pull-right" style="text-align:right;"><img src="<?php echo Yii::app()->baseUrl;?>/images/ad_top.png"></div>
	</div>
	<!-- row header -->
	<div class="row" style="padding:0px;">
			<nav class="nav navbar-default job-navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>

				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav" style="font-weight:bold;">
						<li><a href="<?php echo Yii::app()->baseUrl;?>/index.php">HOME</a></li>
						<li><a href="<?php echo Yii::app()->baseUrl;?>/jobsearch">Job Search</a></li>
						<li><a href="<?php echo Yii::app()->baseUrl;?>/employeesearch">Employee Search</a></li>
						<li><a href="#">HELP</a></li>
					</ul>
				
					<div class="pull-right">
						<img src="<?php echo Yii::app()->baseUrl;?>/images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
					</div>
				</div>
			</nav>
	</div>
	<!-- row menu header -->
	<div class="row" style="margin-top:20px;">
		<div class="col-md-12" style="padding: 0px;float:right;height:auto;">
			<nav class="navbar navbar-default narbar-gradient">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>

				     	<a href="#" class="navbar-brand">ยินดีต้อนรับ คุณ <?php echo Yii::app()->session['c_name'];?></a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="font-weight:bold;">
				      <ul class="nav navbar-nav navbar-right">
				        <li><a href="searchemployee">ค้นหาพนักงาน</a></li>
				        <li class="active"><a href="post">ลงประกาศงาน</a></li>
				        <li><a href="interest">สถานะตำแหน่งงาน</a></li>
				        <li><a href="profile">ตั้งค่าบัญชีผู้ใช้งาน</a></li>
				        
				      </ul>
				     
				      
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<div class="row">
					<div class="col-md-12">
						<div id="alertresume"></div>
						<div style="border: 1px solid #dfdfdf;border-radius:4px;padding:20px;">
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #41d6fa;border-radius:6px;margin-bottom:20px;">
									<span class="resume-header"><i class="glyphicon glyphicon-info-sign"></i> 	ลงประกาศงาน</span>
									<form id="frmPostJob">
										<table width="100%" class="job-table">
											<tr>
												<td align="right" width="40%" style="padding-right:10px;">
													หมวดของงาน
												</td>
												<td>
													<select name="selectCateJob" class="form-control input-sm" style="width:250px;">
														<option value="">--</option>
														<?php foreach($j_category as $row_category):?>
															<option value="<?php echo $row_category->id_category;?>"><?php echo $row_category->category_name;?></option>
														<?php endforeach;?>
													</select>
													<input  name="hidden_com_id" type="hidden" value="<?php echo Yii::app()->session['id_company'];?>">
												</td>
											</tr>
											<tr>
												<td align="right">
													ตำแหน่งงาน
												</td>
												<td>
													<input class="form-control input-sm" style="width:250px;" name="job_position" type="text">
												</td>
											</tr>
											<tr>
												<td align="right">
													รายละเอียดตำแหน่งงาน
												</td>
												<td>
													<textarea class="form-control input-sm" style="width:300px;" name="job_detail"></textarea>
												</td>
											</tr>
											<tr>
												<td align="right">คุณสมบัติ</td>
												<td>
													<textarea class="form-control input-sm" style="width:300px;" name="job_qualification"></textarea>
												</td>
											</tr>
											<tr>
												<td align="right">
													อัตราที่รับ
												</td>
												<td>
													<input class="form-control input-sm" style="width:100px;" name="job_receive" type="text">
												</td>
											</tr>
											<tr>
												<td align="right">อัตราเงินเดือน</td>
												<td>
													<input class="form-control input-sm" style="width:100px;" name="job_salary" type="text">
												</td>
											</tr>
											<tr>
												<td align="right">สถานที่</td>
												<td>
													<input class="form-control input-sm" style="width:100px;" name="job_location" type="text">
												</td>
											</tr>
											<tr>
												<td></td>
												<td>
													<span id="divbtn">
													<button class="btn btn-info" style="width:150px;"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>
													</span>
												</td>
											</tr>
										</table>
									</form>
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
						</div>
						<!-- border -->
					</div>
					<!-- col-md-12 -->
				</div>
				<!-- row -->
		</div>
		<!-- col-md-12 -->
	</div>
	<!-- row -->
</div>
<!-- container -->
<div class="container-min" style="margin-top:20px;">
     <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
        <!-- Footer -->
</div>
<!-- container -->
</body>
</html>