<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta charset="utf-8"/>
	<title>JOB Samui</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/assets/bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/mystyle.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/job-style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/js/jquery.form.min.js"></script>
 	<script>
 		$(function() {
                $('#frmLogin').ajaxForm({
                        url: 'Checklogin',
                        type: 'post',
                        dataType: 'html', 
                    beforeSend: function(){
                        //$('#pg').val(0);  //กำหนดค่า progress ให้เป็น 0
                        //$('#pc').html('0%');  //ให้แสดงค่า 0%
                        $('button[name=btnLogin]').html('<img src="../images/ajax-loader-wi.gif"> Signing In'); 

                    },
                    uploadProgress: function(event, position, total, percent){
                        //$('#pg').val(percent);
                        //$('#pc').html(percent + '%');
                         $('button[name=btnLogin]').html('<img src="../images/ajax-loader-wi.gif"> Signing In'); 
                    },
                    success: function(result){
                        //$('#pg').val(100);
                       // $('#pc').html('100%');
                                    
                       // $('#result').html(result);
                        $('#frmLogin')[0].reset();
                        $('button[name=btnLogin]').html('Login');
                        if(result == "ok"){
                        	var alert = '<div class="alert alert-success"><strong>Success!</strong> .</div>';
                        	$('#alertlogin').html(alert);
                        	window.location='resume';
                        }else{
                        	var alert = '<div class="alert alert-danger"><strong>Danger!</strong> email and password not founds.</div>';
                        	$('#alertlogin').html(alert);
                        	$('input[name=txtusername]').focus();
                        }
                       
                        //var reid = $('input[name=academichiddenID]').val()
                       // editAcademic(reid);
                       
                       //window.location='resume.php';
                    },
                    error: function(xhr, textStatus){
                        alert(textStatus);
                    }
                });
            }); 
		function checkvalue(){
			var username = $('input[name=txtusername]').val();
			var password = $('input[name=txtpassword]').val();
			if(username == "" || password == ""){
				var alert = '<div class="alert alert-warning"><strong>Warning!</strong> ยังไม่ได้ป้อน email และ password.</div>';
                $('#alertlogin').html(alert);
                return false;
			}
		}
 	</script>
</head>
<body>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
    <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a style="padding: 10px 15px;" >NEW USER?:</a></li>
                    <li><a style="padding: 10px 15px;" href="#">SIGN UP</a></li>
                    <li><a style="padding: 10px 15px;" href="#">SIGN IN</a></li>
                    <li><a style="padding: 10px 15px;" href="#">HELP</a></li>
                </ul>
            </div>
    </div>
</nav>
<div class="container-min">
    <div class="row">
        <div class="col-md-12 jobheader">
            <div class="pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/job-logo.png"></div>
            <div class="pull-right" style="text-align:right;"><img src="../images/ad_top.png"></div>
        </div>
        <!-- col-md-12 -->
    </div>
    <!-- row header -->
    <div class="row" style="padding:0px;">
            <nav class="nav navbar-default job-navbar">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                    <ul class="nav navbar-nav" style="font-weight:bold;">
                        <li><a href="<?php echo Yii::app()->baseUrl;?>/index.php">HOME</a></li>
                        <li><a href="<?php echo Yii::app()->baseUrl;?>/jobsearch">Job Search</a></li>
                        <li><a href="#">Employee Search</a></li>
                        <li><a href="#">HELP</a></li>
                    </ul>
                
                    <div class="pull-right">
                        <img src="<?php echo Yii::app()->baseUrl;?>/images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
                    </div>
                </div>
            </nav>
    </div>
    <!-- row menu header -->
    <div class="row" style="margin-top:15px;">
        <div class="col-md-3 job-leftbar">
            <div class="row">
                <div class="col-md-12" style="margin-top:20px;">
                    <div class="job-box-header">
                        <span style="margin-bottom:0px;padding-top:30px;font-size:18px;color:#ff9900;">JOB SAMUI:</span><br>
                        <span style="margin-top:-10px;font-size:14px;color:#ffffff;">QUICK JOB SEARCH</span>
                    </div>
                    <!-- job-box-header -->
                    <div class="job-box-content">
                        <div class="row">
                            <div class="col-md-12">
                                <span style="font-weight:bold;">Enter Keyword(s)</span>
                                <div class="form-group">
                                    <input name="txtkeyword" class="form-control input-sm job-input-search" type="text"/>
                                </div>
                            </div>
                            <!-- col-md-12 -->
                        </div>
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12">
                                <span style="font-weight:bold;">Select a Job Category</span>
                                <select name="txtselect" class="form-control input-sm job-input-search">
                                    <option value="">--</option>
                                    <?php foreach($j_category as $row_category):?>
                                        <option value="<?php echo $row_category->id_category;?>"><?php echo $row_category->category_name;?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <!-- col-md-12 -->
                        </div>
                        <!-- row -->
                        <div class="row">
                            <div class="col-md-12" style="text-align:right;">
                                <button class="btn btn-warning" style="font-weight:bold;margin-top:20px;">Search Job</button>
                                <br>
                                <span style="font-family:'TH SarabunPSK';font-size:18px;font-weight:bold;">ค้นหา: งานด่วน</span>
                            </div>
                            <!-- col-md-12 -->
                        </div>
                        <!-- row -->
                    </div>
                    <!-- job-box-content -->
                </div>
                <!-- col-md-12 -->
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-10" style="border: 1px solid #dfdfdf;border-radius:4px;margin:25px;">
                    <?php //$row = getArticlePost();
               // foreach($article as $result): ?>
                <table style="border-bottom: 1px solid #dfdfdf;">
                    <tr>
                        <td><span style="font-size:20px;"></span></td>
                    </tr>
                    <tr>
                        <td><span style="font-size:14px;"></span></td>
                    </tr>
                </table>
                <?php //endforeach; ?>
                </div>
                <!-- col-md-10 -->
            </div>
            <!-- row -->
            <div class="row">
                <div class="col-md-10" style="padding:10px;border: 1px solid #dfdfdf;border-radius:4px;margin:5px 25px;">
                    <img src="<?php echo Yii::app()->baseUrl;?>/images/250-150-ads.png" style="width:100%;">
                </div>
                <!-- col-md-10 -->
            </div>
            <!-- row -->
        </div>
        <!-- col-md-3 job-leftbar -->
        <div class="col-md-9" style="padding: 0px;">
            <div class="row">
                <div class="col-md-12">
                    <div class="job-content">
                    <img src="<?php echo Yii::app()->baseUrl;?>/images/for-member.png" style="float:right;">   
                        <h1>Member Login</h1>       
                        
                        <hr style="border-width:3px;">
                        <div id="alertlogin"></div>
                        <table class="tablecontent" width="100%"> 
                            <form class="form-inline" id="frmLogin" onsubmit="return checkvalue()">
                            <tr>
                                <td width="40%" align="right" height="50">Email / Username :</td>
                                <td><input name="txtusername" class="form-control job-input-search" style="display:inline;width:200px;" type="text"/></td>
                            </tr>
                            <tr>
                                <td align="right" height="50">Password :</td>
                                <td><input name="txtpassword" class="form-control job-input-search" style="display:inline;width:200px;" type="password"/></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <button name="btnLogin" class="btn btn-warning" style="width:150px;font-weight:bold;margin-top:10px;">
                                        
                                        Login
                                    </button>
                                </td>
                            </tr>
                            </form>
                        </table>
                    </div>
                    <!-- job-box-content -->
                </div>
                <!-- col-md-12 -->
            </div>
            <!-- row -->
        </div>
        <!-- col-md-9 -->
    </div>
    <!-- row -->
</div>
<!-- container -->
<div class="container" style="margin-top:20px;">
     <hr>
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
        <!-- Footer -->
</div>
<!-- container -->
</body>
</html>