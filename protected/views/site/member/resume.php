<?php
	if(Yii::app()->session['id_member']==''){
		echo "<meta charset=\"utf-8\"/><script>alert('ยังไม่ได้เข้าสู่ระบบ');window.location='login';</script>";exit();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta charset="utf-8"/>
	<title>JOB Samui</title>
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/assets/bootstrap/css/bootstrap.css">

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/mystyle.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/css/job-style.css">
  	<!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl;?>/assets/fancy/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl;?>/assets/fancy/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  	<script src="<?php echo Yii::app()->baseUrl;?>/assets/js/jquery.form.min.js"></script>
  	<style type="text/css">
	  	.narbar-gradient{
	  		background-color: rgba(194, 188, 188, 1);
	    	background: -webkit-linear-gradient(top, rgba(214, 214, 214, 1) 0%, rgba(194, 188, 188, 1) 100%);
	    	background: linear-gradient(to bottom, rgba(214, 214, 214, 1) 0%, rgba(194, 188, 188, 1) 100%);
	    }
	    .div {
	    	text-align: center;
	    	display: block;
	    	width: :100%;
	    }
	    .div div label{
			display: inline-block;
			width: 250px;
			text-align: right;
			padding-right: 10px;
		} 
		.div div{
			padding-bottom: 10px;
		}
		.div div div{
			width: 400px;
			display: inline-block;
			text-align: left;
		}
		.job-table tr td{
			padding: 5px 10px;
		}
		.resume-header{
			text-align:center;
			font-size:24px;
			border-bottom-right-radius: 6px;
			border-bottom-left-radius: 6px;
			background-color:#ff9900;
			display: inline-block;
			width: 250px;
			color:#ffffff;
			margin-bottom: 10px;
		}
  	</style>
  	<script>
  		 $(function(){
	        $("#fancybox-manual-b").click(function() {
	            var id = $('input[name=Re_members_id]').val();
	                $.fancybox.open({
	                    href : 'fileupload?id='+id,
	                    type : 'iframe',
	                    height: 200,
	                    padding : 5
	                });
	        });
	        $('#frmResume').ajaxForm({
	        	url: 'process/resumeSave.php',
	        	type: 'post',
	        	dataType: 'html',
	        	beforeSend: function(){
					//$('#pg').val(0);  //กำหนดค่า progress ให้เป็น 0
		       	 	//$('#pc').html('0%');  //ให้แสดงค่า 0%
		       	 	var btn = '<button class="btn btn-warning btn-sm" style="width:100px;" disabled><img src="image/ajax-loader-wi.gif"/> Saving...</button>';
		       	 	$('#divbtn').html(btn);
		  	  	},
		  	  	uploadProgress: function(event, position, total, percent){
					//$('#pg').val(percent);
		       	 	//$('#pc').html(percent + '%');
		       	 	var btn = '<button class="btn btn-warning btn-sm" style="width:100px;" disabled><img src="image/ajax-loader-wi.gif"/> Saving...</button>';
		       	 	$('#divbtn').html(btn);
		    	},
		    	success: function(result){
					//$('#pg').val(100);
		        	//$('#pc').html('100%');
					//$.fancybox.close();
					//$('#result').html(result);
					var btn = '<button class="btn btn-warning btn-sm" style="width:100px;"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>';
		       	 	$('#divbtn').html(btn);
					$('#frmResume')[0].reset();
					//var psnid = $('input[name=psnid]').val();
					//parent.jQuery.fancybox.close();
					//parent.$('#imageprofile').html(result);
					//parent.editPersonnel(psnid);
					window.location='resume.php';
		    	},
				error: function(xhr, textStatus){
					alert(textStatus);
				}
	        });
	    });

  	</script>
</head>
<body>
<nav class="nav navbar-default" style="background-color:#FFFFFF;">
	<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			    </button>
			    <!--<a class="navbar-brand" href="#">jobsamui.com</a>-->
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
				          <a style="padding: 10px 15px;" href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo Yii::app()->baseUrl;?>/images/discovered.png" style="width:20px;"> <?php echo Yii::app()->session['email'];?> <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="#">ค้นหางาน</a></li>
				            <li><a href="resume">แก้ไขประวัติ</a></li>
				            <li><a href="#">งานที่สมัคร</a></li>
				            <li><a href="#">บริษัทที่ดูประวัติ</a></li>
				            <li><a href="#">ตั้งค่าบัญชีผู้ใช้งาน</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="logout.php">ออกจากระบบ</a></li>
				          </ul>
				    </li>
				</ul>
			</div>
	</div>
</nav>
<!-- top menu bar -->
<div class="container-min">
	<div class="row jobheader">
			<div class="pull-left"><img src="<?php echo Yii::app()->baseUrl;?>/images/job-logo.png"></div>
			<div class="pull-right" style="text-align:right;"><img src="<?php echo Yii::app()->baseUrl;?>/images/ad_top.png"></div>
	</div>
	<!-- row header -->
	<div class="row" style="padding:0px;">
			<nav class="nav navbar-default job-navbar">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				    </button>

				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
					<ul class="nav navbar-nav" style="font-weight:bold;">
						<li><a href="<?php echo Yii::app()->baseUrl;?>/index.php">HOME</a></li>
						<li><a href="<?php echo Yii::app()->baseUrl;?>/jobsearch">Job Search</a></li>
						<li><a href="#">SIGN IN</a></li>
						<li><a href="#">HELP</a></li>
					</ul>
				
					<div class="pull-right">
						<img src="<?php echo Yii::app()->baseUrl;?>/images/nav-bar-logo.png" style="margin-right:10px;margin-top:10px;">
					</div>
				</div>
			</nav>
	</div>
	<!-- row menu header -->
	<div class="row" style="margin-top:20px;">
		
		<div class="col-md-12" style="padding: 0px;float:right;height:auto;">
			
				<nav class="navbar navbar-default narbar-gradient">
				  <div class="container-fluid">
				    <!-- Brand and toggle get grouped for better mobile display -->
				    <div class="navbar-header">
				      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-3" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				     	<a href="#" class="navbar-brand">ยินดีต้อนรับ คุณ <?php echo Yii::app()->session['name'];?></a>
				    </div>

				    <!-- Collect the nav links, forms, and other content for toggling -->
				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3" style="font-weight:bold;">
				      <ul class="nav navbar-nav navbar-right">
				        <li><a href="member_job_search.php">ค้นหางาน</a></li>
				        <li class="active"><a href="resume.php">แก้ไขประวัติ</a></li>
				        <li><a href="#">งานที่สมัคร</a></li>
				        <li><a href="#">บริษัทที่ดูประวัติ</a></li>
				        <li><a href="#">ตั้งค่าบัญชีผู้ใช้งาน</a></li>
				        <!--
				        <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
				          <ul class="dropdown-menu">
				            <li><a href="#">Action</a></li>
				            <li><a href="#">Another action</a></li>
				            <li><a href="#">Something else here</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="#">Separated link</a></li>
				            <li role="separator" class="divider"></li>
				            <li><a href="#">One more separated link</a></li>
				          </ul>
				        </li>-->
				      </ul>
				     
				      
				    </div><!-- /.navbar-collapse -->
				  </div><!-- /.container-fluid -->
				</nav>
				<div class="row">
					<div class="col-md-12">
						<div id="alertresume"></div>
						<div style="border: 1px solid #dfdfdf;border-radius:4px;padding:20px;">
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;">
								<span class="resume-header"><i class="glyphicon glyphicon-info-sign"></i> สถานะ</span>
									<form id="frmResume">
									<table width="100%" class="job-table">
										<tr>
											<td align="right" width="40%" style="padding-right:10px;">Resume Status(สถานะประวัติ)</td>
											<td>
												<input name="Re_Status" type="radio" value="enable" <?php if($resultmem['resume_status']=='enable'){echo "checked";}?>/> แสดงประวัติ
												<input name="Re_Status" type="radio" value="disable" <?php if($resultmem['resume_status']=='disable'){echo "checked";}?>/> ซ่อนประวัติ
											
												<input name="Re_members_id" class="form-control input-sm" type="hidden" value="<?php echo $resultmem['id_member'];?>" />
											</td>
										</tr>
										<tr>
											<td align="right" style="padding-right:10px;">วิชาชีพ(vocation)</td>
											<td>
												<select name="Re_vocation" class="form-control input-sm" style="width:250px;">
												<option value="">--</option>
												
												</select>
											</td>
										</tr>
										<tr>
											<td></td>
											<td align="left">
												<div id="divbtn">
													<button class="btn btn-warning btn-sm" style="width:100px;"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>
												</div>
											</td>
										</tr>
									</table>
									</form>	
									
									
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;">
									<span class="resume-header"><i class="glyphicon glyphicon-pushpin"></i> ประวัติส่วนตัว</span>
									<table class="job-table" width="100%">
										<tr>
											<td align="right" width="40%">ชื่อ(name)</td>
											<td>
												
												<input name="Re_name" class="form-control input-sm" style="width:200px;display:inline;" type="text" placeholder="Firstname" value="<?php echo $resultmem->name;?>" />
											</td>
											<td rowspan="5">
												<div id="imageprofile">
												<?php if($resultmem['image']==""){ echo '<img src="../images/profile.jpg" style="width:120px;">';}else{echo '<img src="../upload/'.$resultmem->image.'" style="width:120px;">';}?>
													</div><br>
												<button id="fancybox-manual-b" class="btn btn-warning btn-sm"><i class="glyphicon glyphicon-open-file"></i> Upload Image</button>
											</td>
										</tr>
										<tr>
											<td align="right">นาทสกุล(surname)</td>
											<td><input name="Re_surname" class="form-control input-sm" style="width:200px;display:inline;" type="text" placeholder="Lastname" value="<?php echo $resultmem->surname;?>" /></td>
										</tr>
										<tr>
											<td align="right">เพศ(gender)</td>
											<td>
												<input name="Re_gender"  type="radio" value="Male" <?php if($resultmem->my_qualification->gender=='Male'){echo "checked";}?>/>Male(ชาย)
												<input name="Re_gender"  type="radio" value="Female" <?php if($resultmem->my_qualification->gender=='Female'){echo "checked";}?>/>Female(หญิง)
											</td>
										</tr>
										<tr>
											<td align="right">วันเดือนปีเกิด(birthdate)</td>
											<td>
												<select name="Re_date" class="form-control input-sm" style="width:70px;display:inline;" >';
													<option value="">--</option>
													<option value="01" <?php if($birth[2]=='01'){echo "selected";}?>>1</option>
													<option value="02" <?php if($birth[2]=='02'){echo "selected";}?>>2</option>
													<option value="03" <?php if($birth[2]=='03'){echo "selected";}?>>3</option>
													<option value="04" <?php if($birth[2]=='04'){echo "selected";}?>>4</option>
													<option value="05" <?php if($birth[2]=='05'){echo "selected";}?>>5</option>
													<option value="06" <?php if($birth[2]=='06'){echo "selected";}?>>6</option>
													<option value="07" <?php if($birth[2]=='07'){echo "selected";}?>>7</option>
													<option value="08" <?php if($birth[2]=='08'){echo "selected";}?>>8</option>
													<option value="09" <?php if($birth[2]=='09'){echo "selected";}?>>9</option>
													<?php
													$birth = explode("-", $resultmem->my_qualification->birth_date);
													echo $birth[0];
													for($i=10;$i<=31;$i++){
														if($i==$birth[2]){
															echo '<option value="'.$i.'" selected>'.$i.'</option>';
														}else{
															echo '<option value="'.$i.'">'.$i.'</option>';
														}
													}
													?>
												</select>
												<select name="Re_month" class="form-control input-sm" style="width:120px;display:inline;" >
													<option value="">--</option>
													<option value="01" <?php if($birth[1]=='01'){echo "selected";}?>>มกราคม</option>
													<option value="02" <?php if($birth[1]=='02'){echo "selected";}?>>กุมภาพันธ์</option>
													<option value="03" <?php if($birth[1]=='03'){echo "selected";}?>>มีนาคม</option>
													<option value="04" <?php if($birth[1]=='04'){echo "selected";}?>>เมษายน</option>
													<option value="05" <?php if($birth[1]=='05'){echo "selected";}?>>พฤษภาคม</option>
													<option value="06" <?php if($birth[1]=='06'){echo "selected";}?>>มิถุนายน</option>
													<option value="07" <?php if($birth[1]=='07'){echo "selected";}?>>กรกฎาคม</option>
													<option value="08" <?php if($birth[1]=='08'){echo "selected";}?>>สิงหาคม</option>
													<option value="09" <?php if($birth[1]=='09'){echo "selected";}?>>กันยายน</option>
													<option value="10" <?php if($birth[1]=='10'){echo "selected";}?>>ตุลาคม</option>
													<option value="11" <?php if($birth[1]=='11'){echo "selected";}?>>พฤศจิกายน</option>
													<option value="12" <?php if($birth[1]=='12'){echo "selected";}?>>ธันวาคม</option>
												</select>
												<select name="Re_year" class="form-control input-sm" style="width:85px;display:inline;">
													<option value="">--</option>
													<?php 
													for($i=1930;$i<=2012;$i++){
														if($i==$birth[0]){
															echo '<option value="'.$i.'" selected>'.$i.'</option>';
														}else{
															echo '<option value="'.$i.'">'.$i.'</option>';
														}
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">ที่อยู่(address)</td>
											<td><textarea name="Re_address" class="form-control input-sm" style="width:200px;display:inline;"><?php echo $resultmem->address;?></textarea></td>
										</tr>
										<tr>
											<td align="right">เบอร์ติดต่อ(tel)</td>
											<td><input name="Re_tel" class="form-control input-sm" type="text" value="<?php echo $resultmem->phone;?>" style="width:200px;display:inline;"/></td>
										</tr>
										<tr>
											<td align="right">สถานภาพสมรส</td>
											<td colspan="2">
												<select name="Re_status" class="form-control input-sm" style="width:100px;display:inline;">
													<option value="">--</option>
													<option value="โสด" <?php if($resultmem->my_qualification->status=='โสด'){echo "selected";}?>>โสด</option>
													<option value="สมรส" <?php if($resultmem->my_qualification->status=='สมรส'){echo "selected";}?>>สมรส</option>
													<option value="หย่า" <?php if($resultmem->my_qualification->status=='หย่า'){echo "selected";}?>>หย่า</option>
													<option value="หม้าย" <?php if($resultmem->my_qualification->status=='หม้าย'){echo "selected";}?>>หม้าย</option>
													<option value="อื่นๆ" <?php if($resultmem->my_qualification->status=='อื่นๆ'){echo "selected";}?>>อื่นๆ</option>
												</select>
												<span style="margin-left:40px;margin-right:10px;">ศาสนา</span>
												<input name="Re_religion" class="form-control input-sm" type="text" value="<?php echo $resultmem->my_qualification->religion;?>" style="width:100px;display:inline;"/>
											</td>
										</tr>
										<tr>
											<td align="right">สัญชาติ</td>
											<td colspan="2">
												<input name="Re_nationality" class="form-control input-sm" type="text" value="<?php echo $resultmem->my_qualification->nationality;?>" style="width:100px;display:inline;" />
												<span style="margin-left:33px;margin-right:10px;">เชื้อชาติ</span>
												<input name="Re_race" class="form-control input-sm" type="text" value="<?php echo $resultmem->my_qualification->race;?>" style="width:100px;display:inline;" />
											</td>
										</tr>
										<tr>
											<td align="right">น้ำหนัก</td>
											<td colspan="2">
												<input name="Re_weight" class="form-control input-sm" type="text" value="<?php echo $resultmem->my_qualification->weight;?>" style="width:100px;display:inline;"/>
												<span style="margin-left:40px;margin-right:10px;">ส่วนสูง</span>
												<input name="Re_height" class="form-control input-sm" type="text" value="<?php echo $resultmem->my_qualification->height;?>" style="width:100px;display:inline;" />
											</td>
										</tr>
										
										<tr>
											<td align="right">สถานภาพทางทหาร</td>
											<td>
												<select name="Re_soldier"  class="form-control input-sm" style="display:inline;width:250px;">
													<option value="">--</option>
													<option value="ผ่านการเกณฑ์ทหารแล้ว" <?php if($resultmem->my_qualification->solder_status=='ผ่านการเกณฑ์ทหารแล้ว'){echo "selected";}?>>ผ่านการเกณฑ์ทหารแล้ว</option>
													<option value="ได้รับการยกเว้น/จบหลักสูตรรักษาดินแดน(รด.)" <?php if($resultmem->my_qualification->solder_status=='ได้รับการยกเว้น/จบหลักสูตรรักษาดินแดน(รด.)'){echo "selected";}?>>ได้รับการยกเว้น/จบหลักสูตรรักษาดินแดน(รด.)</option>
													<option value="ยังไม่ผ่านการเกณฑ์ทหาร" <?php if($resultmem->my_qualification->solder_status=='ยังไม่ผ่านการเกณฑ์ทหาร'){echo "selected";}?>>ยังไม่ผ่านการเกณฑ์ทหาร</option>
												</select>
											</td>
										</tr>
										<tr>
											<td align="right">ประสบการณ์การทำงาน</td>
											<td>
												<input name="Re_experience" class="form-control input-sm" type="text" value="<?php echo $resultmem->my_qualification->experience;?>" style="width:50px;display:inline;" /> ปี
											</td>
										</tr>
										<tr>
											<td></td>
											<td align="left"><button class="btn btn-warning btn-sm" style="width:100px;"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button></td>
										</tr>
									</table>
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;">
									<span class="resume-header"><i class="glyphicon glyphicon-align-left"></i> ความต้องการ</span>
									<table class="job-table" width="100%">
										<tr>
											<td width="25%" align="right">ตำแหน่งที่ต้องการ</td>
											<td>
												1.<input name="Re_" class="form-control input-sm" type="text" value="" style="width:150px;display:inline;" />
												2.<input name="Re_" class="form-control input-sm" type="text" value="" style="width:150px;display:inline;" />
												3.<input name="Re_" class="form-control input-sm" type="text" value="" style="width:150px;display:inline;" />
											</td>
										</tr>
										<tr>
											<td align="right">เงินเดือนที่ต้องการ</td>
											<td>
												<input name="Re_" class="form-control input-sm" type="text" value="" style="width:150px;display:inline;" />
												บาท/เดือน
											</td>
										</tr>
										<tr>
											<td align="right">สถานที่ต้องการทำงาน</td>
											<td>
												<input name="Re_" class="form-control input-sm" type="text" value="" style="width:300px;display:inline;" />
											</td>
										</tr>
										<tr>
											<td align="right">ลักษณะงาน</td>
											<td>
												<input name="Re_" type="checkbox" /> งานประจำ (Full Time) 
												<input name="Re_" type="checkbox" /> งานนอกเวลา (Part Time) <br>
												<input name="Re_" type="checkbox" /> งานอิสระ (Freelance) 
												<input name="Re_" type="checkbox" /> งานสำหรับนักศึกษาฝึกงาน (Training)
											</td>
										</tr>
										<tr>
											<td></td>
											<td>
												<button class="btn btn-warning btn-sm" style="width:100px;"><i class="glyphicon glyphicon-floppy-disk"></i> บันทึก</button>
											</td>
										</tr>
									</table>
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;">
									<span class="resume-header"><i class="glyphicon glyphicon-folder-open"></i>  การศึกษา</span>
									<table class="table table-bordered" width="100%">
										<tr>
											<th width="5%" style="text-align:center;">ลำดับ</th>
											<th width="15%" style="text-align:center;">ระดับการศึกษา</th>
											<th width="30%" style="text-align:center;">สาขาวิชา</th>
											<th style="text-align:center;">สถาบัน</th>
											<th width="13%" style="text-align:center;">ปีที่จบการศึกษา</th>
										</tr>
										<tr>
											<td align="center">1</td>
											<td>
												<select class="class-control input-sm">
													<option value="">--ระดับการศึกษา--</option>
													<option value="ต่ำกว่ามัธยม">ต่ำกว่ามัธยม</option>
													<option value="มัธยมศึกษาตอนต้น">มัธยมศึกษาตอนต้น</option>
													<option value="มัธยมศึกษาตอนปลาย">มัธยมศึกษาตอนปลาย</option>
													<option value="ปวช.">ปวช.</option>
													<option value="ปวส.">ปวส.</option>
													<option value="ปริญญาตรี">ปริญญาตรี</option>
													<option value="ปริญญาโท">ปริญญาโท</option>
													<option value="ปริญญาเอก">ปริญญาเอก</option>
												</select>
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
										</tr>
										<tr>
											<td align="center">2</td>
											<td>
												<select class="class-control input-sm">
													<option value="">--ระดับการศึกษา--</option>
													<option value="ต่ำกว่ามัธยม">ต่ำกว่ามัธยม</option>
													<option value="มัธยมศึกษาตอนต้น">มัธยมศึกษาตอนต้น</option>
													<option value="มัธยมศึกษาตอนปลาย">มัธยมศึกษาตอนปลาย</option>
													<option value="ปวช.">ปวช.</option>
													<option value="ปวส.">ปวส.</option>
													<option value="ปริญญาตรี">ปริญญาตรี</option>
													<option value="ปริญญาโท">ปริญญาโท</option>
													<option value="ปริญญาเอก">ปริญญาเอก</option>
												</select>
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
										</tr>
										<tr>
											<td align="center">3</td>
											<td>
												<select class="class-control input-sm">
													<option value="">--ระดับการศึกษา--</option>
													<option value="ต่ำกว่ามัธยม">ต่ำกว่ามัธยม</option>
													<option value="มัธยมศึกษาตอนต้น">มัธยมศึกษาตอนต้น</option>
													<option value="มัธยมศึกษาตอนปลาย">มัธยมศึกษาตอนปลาย</option>
													<option value="ปวช.">ปวช.</option>
													<option value="ปวส.">ปวส.</option>
													<option value="ปริญญาตรี">ปริญญาตรี</option>
													<option value="ปริญญาโท">ปริญญาโท</option>
													<option value="ปริญญาเอก">ปริญญาเอก</option>
												</select>
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
											<td>
												<input name="" class="form-control input-sm" type="text" />
											</td>
										</tr>
									</table>
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;">
									<span class="resume-header" style="width:300px;"><i class="glyphicon glyphicon-folder-open"></i>  ความสามารถทางภาษา</span>
									<table class="table table-bordered" width="100%">
										<tr>
											<th style="text-align:center;">ภาษา</th>
											<th style="text-align:center;">พูด</th>
											<th style="text-align:center;">ฟัง</th>
											<th style="text-align:center;">อ่าน</th>
											<th style="text-align:center;">เขียน</th>
										</tr>
										<tr>
											<td></td>
											<td width="17%"></td>
											<td width="17%"></td>
											<td width="17%"></td>
											<td width="17%"></td>
										</tr>
									</table>
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
							<div class="row">
								<div class="col-md-1"></div>
								<div class="col-md-10" style="border: 2px solid #ff9900;border-radius:6px;margin-bottom:20px;">
									<span class="resume-header"><i class="glyphicon glyphicon-folder-open"></i>  ความสามารถอื่น</span>
									<table class="job-table" width="100%">
										<tr>
											<td align="right" width="40%"></td>
											<td>
												
											</td>
										</tr>
										<tr>
											<td align="right">อื่น</td>
											<td>
												1. 
											</td>
										</tr>
									</table>
								</div>
								<!-- col-md-10 -->
							</div>
							<!-- row -->
						</div>
					</div>
					<!-- col-md-12 -->
				</div>
				<!-- row -->
				
				
			
		</div>
		<!-- col-md-12 -->
	</div>
	<!-- row -->
	 <hr>
	 <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
            
        </footer>
</div>
<!-- container -->
</body>
</html>